﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FileUploadDemo.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace FileUploadDemo.Pages
{
    public class UploadModel : PageModel
    {
        private readonly FileUploadDemoContext _context;

        public UploadModel(FileUploadDemoContext context)
        {
            _context = context;
        }

        [BindProperty]
        public FileUpload FileUpload { get; set; }

        public async Task<IActionResult> OnGetAsync(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return Page();
            }

            var record = await _context.FileRecords.FirstAsync(r => r.Key == key);
            var stream = new FileStream(record.Path, FileMode.Open);
            return File(stream, "application/octet-stream");
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (await _context.FileRecords.AnyAsync(r => r.Key == FileUpload.Key))
            {
                return StatusCode(409, "The key already exists.");
            }

            var tempPath = Path.GetTempFileName();
            using (var stream = new FileStream(tempPath, FileMode.Create))
            {
                await FileUpload.File.CopyToAsync(stream);
            }

            _context.FileRecords.Add(new FileRecord
            {
                Key = FileUpload.Key,
                Path = tempPath
            });
            _context.SaveChanges();
            return Page();
        }
    }
}