﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileUploadDemo.Models
{
    public class FileRecord
    {
        public int ID { get; set; }
        public string Key { get; set; }
        public string Path { get; set; }
    }
}
