﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace FileUploadDemo.Models
{
    public class FileUpload
    {
        [Required]
        [StringLength(60, MinimumLength = 3)]
        public string Key { get; set; }

        [Required]
        public IFormFile File { get; set; }
    }
}
