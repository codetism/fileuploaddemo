﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace FileUploadDemo.Models
{
    public class FileUploadDemoContext : DbContext
    {
        public FileUploadDemoContext (DbContextOptions<FileUploadDemoContext> options)
            : base(options)
        {
        }

        public DbSet<FileRecord> FileRecords { get; set; }
    }
}
